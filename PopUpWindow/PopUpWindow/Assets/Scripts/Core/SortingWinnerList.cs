﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SortingWinnerList
{
    public List<WinnerRowData> SortWinnerList(List<WinnerRowData> winnerList)
    {       
        ScoreCompare sc = new ScoreCompare();

        winnerList.Sort(sc);
      
        return winnerList;
    }
}

class ScoreCompare : IComparer<WinnerRowData>
{
    public int Compare(WinnerRowData i, WinnerRowData j)
    {
        if(i.score > j.score)
        {
            return -1;
        }
        if(i.score < j.score)
        {
            return 1;
        }
        return 0;
    }
}
