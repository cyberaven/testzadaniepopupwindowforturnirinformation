﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinnerListCreator : MonoBehaviour
{
    List<WinnerRowData> winnerList = new List<WinnerRowData>();
    [SerializeField] private int rowCount = 100;

    void Start()
    {
        MainScreen.CreateNewListEvent += CreateNewList;

        CreateNewList();
    }
    private void OnDestroy()
    {
        MainScreen.CreateNewListEvent -= CreateNewList;
    }
    private void CreateNewList()
    {
        winnerList = new List<WinnerRowData>();

        for (int i = 0; i < rowCount -1 ; i++)
        {
            //создаем рандомный список
            int randId = Random.Range(0, 999999);
            string randFName = RandName();
            string randLName = RandName();
            int randScore = Random.Range(0, 9999);

            winnerList.Add(new WinnerRowData(randId, randFName, randLName, randScore));
        }

        //создаем игрока и добавляем в список
        int plrId = GameCore.INSTANCE.GameSettingData.PlrID;
        string plrFName = RandName();
        string plrLName = RandName();
        int plrScore = Random.Range(0, 9999);
        winnerList.Add(new WinnerRowData(plrId, plrFName, plrLName, plrScore));

        ListToJSON(winnerList);
    }
    private string RandName()
    {
        string result = "RandName";
        List<string> names = new List<string>() { "aaron", "abdul", "abe", "abel", "abraham", "adam", "adan", "adolfo", "adolph", "adrian", "abby", "abigail", "adele", "adrian", "abbott", "acosta", "adams", "adkins", "aguilar" };
        return result = names[Random.Range(0, names.Count)];
    }
    private void ListToJSON(List<WinnerRowData> winners)
    {
        JSONWorker jSONWorker = new JSONWorker();
        jSONWorker.Save(winners);
    }
}
