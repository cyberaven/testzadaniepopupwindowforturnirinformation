﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;

[Serializable]
public class JSONWorker
{
    string path = Application.dataPath + GameCore.INSTANCE.GameSettingData.WinnerTableJSONPath;
    
    public void Save(List<WinnerRowData> winners)
    {
        DeleteOldFile();

        WinnerRowData[] winnersArr = winners.ToArray();
        string json = JsonHelper.ToJson(winnersArr, true);

        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(json);
        writer.Close();
    }
    public List<WinnerRowData> Load()
    {
        string contents = File.ReadAllText(path);
        WinnerRowData[] arr = JsonHelper.FromJson<WinnerRowData>(contents);

        List<WinnerRowData> result = new List<WinnerRowData>(arr);      
        return result;
    }

    private void DeleteOldFile()
    {
        File.Delete(path);
    }
}


public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}
