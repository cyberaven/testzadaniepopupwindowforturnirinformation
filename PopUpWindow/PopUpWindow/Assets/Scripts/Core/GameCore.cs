﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCore : MonoBehaviour
{
    public static GameCore INSTANCE;

    [SerializeField] private GameSettingData gameSettingData;
    public GameSettingData GameSettingData { get => gameSettingData; }

    [SerializeField] WinnerListCreator winnerListCreator;

    

    private void Awake()
    {
        INSTANCE = this;
    }

    void Start()
    {
        winnerListCreator = Instantiate(winnerListCreator, transform);
    }
}
