﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "New GameSettingData", menuName = "GameSettingData", order = 51)]
public class GameSettingData : ScriptableObject
{
    [SerializeField]
    private string winnerTableJSONPath;
    public string WinnerTableJSONPath { get => winnerTableJSONPath;}
    
    [SerializeField]
    private int plrID = 98989898;
    public int PlrID { get => plrID;}

}
