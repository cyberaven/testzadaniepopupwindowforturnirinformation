﻿using System;

[Serializable]
public class WinnerRowData
{
    public int id;
    public string firstName;
    public string lastName;
    public int score;
    public WinnerRowData(int id, string firstName, string lastName, int score)
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.score = score;
    }
}
