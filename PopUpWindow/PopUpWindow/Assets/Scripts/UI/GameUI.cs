﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    [SerializeField] MainScreen mainScreen;
    [SerializeField] PopUpWindow popUpWindow;

    private void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;        
       
        popUpWindow = Instantiate(popUpWindow, transform);
        mainScreen = Instantiate(mainScreen, transform);

        MainScreen.ShowPopUpEvent += ShowPopUpWindow;
    }
    private void OnDestroy()
    {
        MainScreen.ShowPopUpEvent -= ShowPopUpWindow;
    }

    private void ShowPopUpWindow()
    {
        popUpWindow.ShowPopUpBtnClk();
    }
}
