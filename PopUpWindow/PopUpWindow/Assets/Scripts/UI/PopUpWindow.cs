﻿using System.Collections.Generic;
using UnityEngine;

public class PopUpWindow : MonoBehaviour
{
    [SerializeField] private GameObject content;
    [SerializeField] private GameObject userResultat;

    [SerializeField] private ScrollViewRow scrollViewRowPrefab;
    [SerializeField] private List<WinnerRowData> winnerList = new List<WinnerRowData>();

    int currentNumbRow = 1;
    int howMuchLoadRow = 50;

    [SerializeField] private int maxRowCount = 100;

    private void Start()
    {
        ScrollbarController.ScrollBarChangeValueEvent += ScrollBarChangeValue;
        Hide();
    }
    private void OnDestroy()
    {
        ScrollbarController.ScrollBarChangeValueEvent -= ScrollBarChangeValue;
    }
    public void ShowPopUpBtnClk()
    {
        if(gameObject.activeSelf == true)
        {
            Hide();
        }
        else
        {
            Show();            
        }
    }
    void Show()
    {
        gameObject.SetActive(true);
        FillWinnerTable();        
    }
    void Hide()
    {
        DelWinnerTable();
        currentNumbRow = 1;
        gameObject.SetActive(false);        
    }
    private void DelWinnerTable()
    {
        ScrollViewRow[] scrollViewRows = content.GetComponentsInChildren<ScrollViewRow>();
        foreach (ScrollViewRow scroll in scrollViewRows)
        {
            Destroy(scroll.gameObject);
        }
        ScrollViewRow[] userRows = userResultat.GetComponentsInChildren<ScrollViewRow>();
        foreach (ScrollViewRow scroll in userRows)
        {
            Destroy(scroll.gameObject);
        }
    }
    private void FillWinnerTable()
    {
        LoadWinnerList();
        SortWinnerTable();
        CreateWinnerRows();
        FindPlr();
    }   
    private void LoadWinnerList()
    {
        JSONWorker jSONWorker = new JSONWorker();
        winnerList = jSONWorker.Load();      
    }
    private void SortWinnerTable()
    {
        SortingWinnerList sortingWinnerList = new SortingWinnerList();
        winnerList = sortingWinnerList.SortWinnerList(winnerList);        
    }
    private void CreateWinnerRows()
    {
        for (int i = currentNumbRow; i < howMuchLoadRow + currentNumbRow; i++)
        {
            if (i < winnerList.Count + 1 && i <= maxRowCount)
            {
                ScrollViewRow scrollViewRow = Instantiate(scrollViewRowPrefab, content.transform);
                scrollViewRow.Setup(i, winnerList[i - 1]);
            }            
        }
        currentNumbRow += howMuchLoadRow;        
    }
    private void ScrollBarChangeValue()
    {
        CreateWinnerRows();
    }
    private void FindPlr()
    {
        for (int i = 0; i < winnerList.Count; i++)
        {
            if (winnerList[i].id == GameCore.INSTANCE.GameSettingData.PlrID)//ищем игрока
            {
                ScrollViewRow scrollViewRow = Instantiate(scrollViewRowPrefab, userResultat.transform);
                scrollViewRow.Setup(i + 1, winnerList[i]);
            }
        }
    }
}
