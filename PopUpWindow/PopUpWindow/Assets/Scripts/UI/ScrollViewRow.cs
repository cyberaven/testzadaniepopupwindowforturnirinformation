﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewRow : MonoBehaviour
{
    [SerializeField] Text numb;
    [SerializeField] Text fName;
    [SerializeField] Text lName;
    [SerializeField] Text score;

    public void Setup(int numb, WinnerRowData winnerRow)
    {
        this.numb.text = numb.ToString();
        this.fName.text = winnerRow.firstName;
        this.lName.text = winnerRow.lastName;
        this.score.text = winnerRow.score.ToString();
    }
}
