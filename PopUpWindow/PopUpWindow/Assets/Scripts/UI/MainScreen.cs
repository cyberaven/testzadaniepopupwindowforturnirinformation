﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainScreen : MonoBehaviour
{
    [SerializeField] Button createNewList;
    [SerializeField] Button showPopUp;    

    public delegate void CreateNewListDel();
    public static event CreateNewListDel CreateNewListEvent;

    public delegate void ShowPopUpDel();
    public static event ShowPopUpDel ShowPopUpEvent;

    void Start()
    {
        createNewList.onClick.AddListener(CreateNewListClk);
        showPopUp.onClick.AddListener(ShowPopUp);
    }
    private void CreateNewListClk()
    {
        if(CreateNewListEvent != null)
        {
            CreateNewListEvent();
        }
    }
    private void ShowPopUp()
    {
        if(ShowPopUpEvent != null)
        {
            ShowPopUpEvent();
        }
    }



}
