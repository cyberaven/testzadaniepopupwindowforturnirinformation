﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollbarController : MonoBehaviour
{
    Scrollbar sc;
    [SerializeField] float minValue = 0.3f;

    public delegate void ScrollBarChangeValueDel();
    public static event ScrollBarChangeValueDel ScrollBarChangeValueEvent;

    private void OnEnable()
    {
        sc = GetComponent<Scrollbar>();
        sc.value = 1;
    }

    private void Start()
    {
        sc.onValueChanged.AddListener(ScrollbarCallBack);
    }
    private void ScrollbarCallBack(float value)
    {
        if(value < minValue)
        {            
            if(ScrollBarChangeValueEvent != null)
            {
                ScrollBarChangeValueEvent();
            }
        }
    }
}
